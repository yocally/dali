package inflate

// Mod represents an SI prefix with all the needed info to mod a def
type Mod struct {
	SpecLong  string
	SpecShort string
	Mod       int
}

// BuildMods Generate a list of all positive mods
func BuildMods() []Mod {
	var ret []Mod

	ret = append(ret, Mod{SpecLong: "deca", SpecShort: "da", Mod: 1})
	ret = append(ret, Mod{SpecLong: "hecto", SpecShort: "h", Mod: 2})
	ret = append(ret, Mod{SpecLong: "kilo", SpecShort: "k", Mod: 3})
	ret = append(ret, Mod{SpecLong: "mega", SpecShort: "M", Mod: 6})
	ret = append(ret, Mod{SpecLong: "giga", SpecShort: "G", Mod: 9})
	ret = append(ret, Mod{SpecLong: "tera", SpecShort: "T", Mod: 12})
	ret = append(ret, Mod{SpecLong: "peta", SpecShort: "P", Mod: 15})
	ret = append(ret, Mod{SpecLong: "exa", SpecShort: "E", Mod: 18})
	ret = append(ret, Mod{SpecLong: "zetta", SpecShort: "Z", Mod: 21})
	ret = append(ret, Mod{SpecLong: "yotta", SpecShort: "Y", Mod: 24})

	ret = append(ret, Mod{SpecLong: "deci", SpecShort: "d", Mod: -1})
	ret = append(ret, Mod{SpecLong: "centi", SpecShort: "c", Mod: -2})
	ret = append(ret, Mod{SpecLong: "milli", SpecShort: "m", Mod: -3})
	ret = append(ret, Mod{SpecLong: "micro", SpecShort: "μ", Mod: -6})
	ret = append(ret, Mod{SpecLong: "nano", SpecShort: "n", Mod: -9})
	ret = append(ret, Mod{SpecLong: "pico", SpecShort: "p", Mod: -12})
	ret = append(ret, Mod{SpecLong: "femto", SpecShort: "f", Mod: -15})
	ret = append(ret, Mod{SpecLong: "atto", SpecShort: "a", Mod: -18})
	ret = append(ret, Mod{SpecLong: "zepto", SpecShort: "z", Mod: -21})
	ret = append(ret, Mod{SpecLong: "yocto", SpecShort: "y", Mod: -24})
	return ret
}
