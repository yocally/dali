package inflate

import (
	"dali/load"
	"dali/tools"
	"strconv"
)

func Inflate(raw load.Def, mods []Mod) []load.Def {
	var ret []load.Def

	ret = append(ret, raw)

	for i := range mods {
		var temp load.Def
		temp.Class = raw.Class
		temp.SpecIn.Long = mods[i].SpecLong + " " + raw.SpecIn.Long
		temp.SpecIn.Short = mods[i].SpecShort + raw.SpecIn.Short
		temp.SpecOut.Long = raw.SpecOut.Long
		temp.SpecOut.Short = raw.SpecOut.Short
		temp.Rate = Shift(raw.Rate, mods[i].Mod)
		ret = append(ret, temp)
	}
	return ret
}

func InflateRev(raw load.Def, mods []Mod) []load.Def {
	var ret []load.Def

	for i := range mods {
		var temp load.Def
		temp.Class = raw.Class
		temp.SpecIn.Long = raw.SpecIn.Long
		temp.SpecIn.Short = raw.SpecIn.Short
		temp.SpecOut.Long = mods[i].SpecLong + " " + raw.SpecOut.Long
		temp.SpecOut.Short = mods[i].SpecShort + raw.SpecOut.Short
		temp.Rate = ShiftRev(raw.Rate, mods[i].Mod)
		ret = append(ret, temp)
	}
	return ret
}

// StripSN takes in a string that is formated with Scientific Notation and returns the stripped number and the exponent to raise it by
func StripSN(raw string) (string, int) {
	var base string
	var expo int
	if raw[len(raw)-4] == 'E' {
		t, _ := strconv.Atoi(raw[len(raw)-3:])
		base = raw[:len(raw)-4]
		expo = t
	}
	return base, expo
}

// Shift will shift the decimal by the given exponent, positive or negative
func Shift(raw string, ex int) string {
	if !tools.RuneInString('.', raw) {
		raw = raw + ".0"
	}
	str := PadZero(raw)

	var location int
	var strip string
	for i := range str {
		if str[i] == '.' {
			location = i
			strip = str[:i] + str[i+1:]
		}
	}

	location += ex
	ret := strip[:location] + "." + strip[location:]
	ret = Trim(ret)
	if ret[0] == '.' {
		ret = "0" + ret
	}
	if ret[len(ret)-1] == '.' {
		ret = ret[:len(ret)-1]
	}
	return ret
}

func ShiftRev(raw string, ex int) string {
	if !tools.RuneInString('.', raw) {
		raw = raw + ".0"
	}
	str := PadZero(raw)

	var location int
	var strip string
	for i := range str {
		if str[i] == '.' {
			location = i
			strip = str[:i] + str[i+1:]
		}
	}

	location -= ex
	ret := strip[:location] + "." + strip[location:]
	ret = Trim(ret)
	if ret[0] == '.' {
		ret = "0" + ret
	}
	if ret[len(ret)-1] == '.' {
		ret = ret[:len(ret)-1]
	}
	return ret
}
