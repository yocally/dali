package inflate

// PadZero addes 24 zero's on both sides of the given string
func PadZero(original string) string {
	var l string
	for i := 0; i < 71; i++ {
		l += "0"
	}
	return l + original + l
}

// Trim removes trailing zeros from strings created with PadZero
func Trim(original string) string {
	var start int
	var end int
	for i := range original {
		if original[i] != '0' {
			start = i
			break
		}
	}

	for i := len(original) - 1; i != 0; i-- {
		if original[i] != '0' {
			end = i
			break
		}
	}

	return original[start : end+1]
}
