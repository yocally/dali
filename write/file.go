package write

import "os"
import "dali/load"

func OpenFile(w *os.File) {
	w.Write([]byte("[\n"))
}

func CloseFile(w *os.File) {
	w.WriteString("]")
}
func WriteDef(w *os.File, d load.Def) {
	w.WriteString("\t[\n")
	w.WriteString("\t\t[\n")
	w.WriteString("\t\t\t\"" + d.SpecIn.Long + "\",\n")
	w.WriteString("\t\t\t\"" + d.SpecIn.Short + "\"\n")
	w.WriteString("\t\t],\n")
	w.WriteString("\t\t[\n")
	w.WriteString("\t\t\t\"" + d.SpecOut.Long + "\",\n")
	w.WriteString("\t\t\t\"" + d.SpecOut.Short + "\"\n")
	w.WriteString("\t\t],\n")
	w.WriteString("\t\t\"" + d.Class + "\",\n")
	w.WriteString("\t\t" + d.Rate + "\n")
	w.WriteString("\t],\n")
}

func WriteLastDef(w *os.File, d load.Def) {
	w.WriteString("\t[\n")
	w.WriteString("\t\t[\n")
	w.WriteString("\t\t\t\"" + d.SpecIn.Long + "\",\n")
	w.WriteString("\t\t\t\"" + d.SpecIn.Short + "\"\n")
	w.WriteString("\t\t],\n")
	w.WriteString("\t\t[\n")
	w.WriteString("\t\t\t\"" + d.SpecOut.Long + "\",\n")
	w.WriteString("\t\t\t\"" + d.SpecOut.Short + "\"\n")
	w.WriteString("\t\t],\n")
	w.WriteString("\t\t\"" + d.Class + "\",\n")
	w.WriteString("\t\t" + d.Rate + "\n")
	w.WriteString("\t]\n")
}
