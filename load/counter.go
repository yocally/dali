package load

import (
	"bytes"
	"io"
	"os"
)

// lineCounter counts all of the lines in the file
func lineCounter(file string) int {
	r, _ := os.Open(file)
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count + 1

		case err != nil:
			return count + 1
		}
	}
}

// DefCounter counts how many valid defs are in the given file (does not validate)
func DefCounter(file string) int {
	count := lineCounter(file)
	count = (count - 2) / 12
	return count
}
