package load

import (
	"encoding/json"
)

// Spec used exclusively for the Def struct to hold identical shit
type Spec struct {
	Long  string
	Short string
}

// Def represents a definition of a conversion... thing
type Def struct {
	SpecIn  Spec
	SpecOut Spec
	Class   string
	Rate    string
}

// ParseFile Returns a def from the given file at the index (starts at 0)
func ParseFile(path string, index int) Def {
	str := PadString(ScanToString(path, index))
	data := []byte(str)

	var s Def
	var a [][]string
	var b []string
	var c []string

	json.Unmarshal(data, &a)
	json.Unmarshal(data, &b)
	json.Unmarshal(data, &c)

	s.SpecIn.Long = a[0][0]
	s.SpecIn.Short = a[0][1]
	s.SpecOut.Long = a[1][0]
	s.SpecOut.Short = a[1][1]
	s.Class = b[2]
	s.Rate = c[3]
	return s
}
