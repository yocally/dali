package load

import (
	"bufio"
	"os"
)

// ScanToString will return a json string from the given file and the index of the set you want
func ScanToString(path string, rawindex int) string {
	rawindex++
	var index int
	// Small compatibility check as the algorithm breaks when you pass it a one
	if rawindex == 1 {
		index = 1
	} else {
		// Indexes start at 0 on the reader, so we have to add one even though the offset is 2 lines
		index = ((rawindex - 1) * 12) + 1
	}
	f, _ := os.Open(path)
	red := bufio.NewScanner(f)

	var compound string
	var taly int
	for red.Scan() {
		if taly >= index {
			if taly < index+12 {
				var str string
				str = red.Text()
				compound += str
			}
		}
		taly++
	}
	if compound[len(compound)-1] == ']' {
		return compound
	}
	return compound[:len(compound)-1]
}

// PadString If given json that has the decimal formated as a number, will pad that decimal with double quotes
func PadString(raw string) string {
	numMap := make(map[rune]bool)
	numMap['0'] = true
	numMap['1'] = true
	numMap['2'] = true
	numMap['3'] = true
	numMap['4'] = true
	numMap['5'] = true
	numMap['6'] = true
	numMap['7'] = true
	numMap['8'] = true
	numMap['9'] = true
	numMap['.'] = true
	numMap['E'] = true
	numMap['+'] = true
	numMap['-'] = true
	var new string
	for v, i := range raw {
		if !numMap[i] {
			continue
		}

		if !numMap[rune(raw[v-1])] {
			new = raw[:v-1] + "\"" + raw[v:]
		}
		if !numMap[rune(raw[v+1])] {
			new = new[:v+1] + "\"" + raw[v+1:]
		}
	}
	return new
}
