package main

import (
	"dali/inflate"
	"dali/load"
	"dali/tools"
	"dali/write"
	"os"
)

func main() {
	infile := "volume_coefficients.json"
	outfile := "volume_inflate.json"
	fileSize := load.DefCounter(infile)
	var fileDefs []load.Def
	mods := inflate.BuildMods()

	for i := 0; i < fileSize; i++ {
		fileDefs = append(fileDefs, load.ParseFile(infile, i))
	}

	// a := inflate.Inflate(fileDefs[0], mods)
	// out, _ := os.Create(outfile)
	// write.OpenFile(out)
	// for i := range a {
	// 	write.WriteDef(out, a[i])
	// }
	// b := inflate.InflateRev(fileDefs[0], mods)
	// for i := range b {
	// 	write.WriteDef(out, b[i])
	// }
	// write.CloseFile(out)

	out, _ := os.Create(outfile)
	write.OpenFile(out)
	for master := range fileDefs {
		top := inflate.Inflate(fileDefs[master], mods)
		var bot []load.Def
		for sub := range top {
			bot = tools.AddSlice(bot, inflate.InflateRev(top[sub], mods))
		}
		for _, a := range top {
			write.WriteDef(out, a)
		}
		for _, a := range bot {
			write.WriteDef(out, a)
		}
	}
	write.CloseFile(out)
}
