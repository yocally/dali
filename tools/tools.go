package tools

import (
	"dali/load"
)

// RuneInString if the given rune exists in the given slice, return true
func RuneInString(a rune, str string) bool {
	for _, b := range str {
		if b == a {
			return true
		}
	}
	return false
}

// StringInSlice if the given string is in the given slice, return true
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func AddSlice(a []load.Def, b []load.Def) []load.Def {
	for i := range b {
		a = append(a, b[i])
	}
	return a
}
